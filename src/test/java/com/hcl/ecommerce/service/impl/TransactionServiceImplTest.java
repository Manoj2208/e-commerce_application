package com.hcl.ecommerce.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyList;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.ecommerce.dto.ApiResponse;
import com.hcl.ecommerce.dto.TransactionDto;
import com.hcl.ecommerce.entity.Cart;
import com.hcl.ecommerce.entity.CartProduct;
import com.hcl.ecommerce.entity.Login;
import com.hcl.ecommerce.entity.Product;
import com.hcl.ecommerce.entity.Transaction;
import com.hcl.ecommerce.entity.User;
import com.hcl.ecommerce.entity.Wallet;
import com.hcl.ecommerce.exception.InsufficientFundException;
import com.hcl.ecommerce.exception.ResourceNotFoundException;
import com.hcl.ecommerce.exception.UnauthorizedCustomer;
import com.hcl.ecommerce.repository.CartRepository;
import com.hcl.ecommerce.repository.LoginRepository;
import com.hcl.ecommerce.repository.ProductRepository;
import com.hcl.ecommerce.repository.TransactionRepository;
import com.hcl.ecommerce.repository.UserRepository;
import com.hcl.ecommerce.repository.WalletRepository;

@ExtendWith(SpringExtension.class)
class TransactionServiceImplTest {
	@Mock
	UserRepository userRepository;
	@Mock
	LoginRepository loginRepository;
	@Mock
	WalletRepository walletRepository;
	@Mock
	ProductRepository productRepository;
	@Mock
	CartRepository cartRepository;
	@Mock
	TransactionRepository transactionRepository;
	@InjectMocks
	TransactionServiceImpl transactionServiceImpl;
	
	@Test
	void testPurchaseProductSuccess() {
		TransactionDto transactionDto=TransactionDto.builder().cartId(1L).userId(1L).walletId(1L).build();
		User existingUser=User.builder().userId(1L).build();
		Login login=Login.builder().user(existingUser).build();
		Product product=Product.builder().productId(123L).availableQuantity(5).cost(100D).build();
		ArrayList<CartProduct> cartProductList=new ArrayList<>();
		cartProductList.add(new CartProduct(1L, 123L, 1));
		ArrayList<Product> products=new ArrayList<>();
		products.add(product);
		Wallet wallet=Wallet.builder().walletId(1L).balance(200D).user(existingUser).build();
		Cart cart=Cart.builder().cartId(1L).user(existingUser).cartProducts(cartProductList).build();
		Transaction transaction=Transaction.builder().build();		
		Mockito.when(userRepository.findById(transactionDto.userId())).thenReturn(Optional.of(existingUser));
		Mockito.when(loginRepository.findByUserAndIsLogin(existingUser,true)).thenReturn(Optional.of(login));
		Mockito.when(cartRepository.findByCartIdAndUser(transactionDto.cartId(),existingUser)).thenReturn(Optional.of(cart));
		Mockito.when(productRepository.findAllById(anyList())).thenReturn(products);
		Mockito.when(walletRepository.findByWalletIdAndUser(transactionDto.walletId(), existingUser)).thenReturn(Optional.of(wallet));	
		Mockito.when(transactionRepository.save(transaction)).thenReturn(transaction);
		Mockito.when(productRepository.saveAll(products)).thenReturn(products);
		Mockito.when(walletRepository.save(wallet)).thenReturn(wallet);	
		ApiResponse apiResponse=transactionServiceImpl.purchaseProduct(transactionDto);
		assertEquals("Product purchased Successfully",apiResponse.message());
		assertEquals(201L, apiResponse.httpStatus());
		assertNotNull(transactionRepository.save(transaction));
		assertNotNull(productRepository.saveAll(products));
		assertNotNull(walletRepository.save(wallet));
		assertEquals(4, product.getAvailableQuantity());
		assertEquals(100, wallet.getBalance());		
	}
	
	
	@Test
	void testPurchaseProductUserNotFound() {
		TransactionDto transactionDto=TransactionDto.builder().cartId(1L).userId(1L).walletId(1L).build();
		Mockito.when(userRepository.findById(transactionDto.userId())).thenReturn(Optional.empty());
		assertThrows(ResourceNotFoundException.class,()->transactionServiceImpl.purchaseProduct(transactionDto));		
	}
	
	@Test
	void testPurchaseProductUserNotLoggedIn() {
		TransactionDto transactionDto=TransactionDto.builder().cartId(1L).userId(1L).walletId(1L).build();
		User existingUser=User.builder().userId(1L).build();
		Mockito.when(userRepository.findById(transactionDto.userId())).thenReturn(Optional.of(existingUser));
		Mockito.when(loginRepository.findByUserAndIsLogin(existingUser,true)).thenReturn(Optional.empty());
		assertThrows(UnauthorizedCustomer.class,()->transactionServiceImpl.purchaseProduct(transactionDto));		
	}
	
	@Test
	void testPurchaseProductCartNotFound() {
		TransactionDto transactionDto=TransactionDto.builder().cartId(1L).userId(1L).walletId(1L).build();
		User existingUser=User.builder().userId(1L).build();
		Login login=Login.builder().user(existingUser).build();
		Mockito.when(userRepository.findById(transactionDto.userId())).thenReturn(Optional.of(existingUser));
		Mockito.when(loginRepository.findByUserAndIsLogin(existingUser,true)).thenReturn(Optional.of(login));
		Mockito.when(cartRepository.findByCartIdAndUser(transactionDto.cartId(),existingUser)).thenReturn(Optional.empty());	
		assertThrows(ResourceNotFoundException.class,()->transactionServiceImpl.purchaseProduct(transactionDto));
	}
	
	@Test
	void testPurchaseProductProductQuantityNotAvailable() {
		TransactionDto transactionDto=TransactionDto.builder().cartId(1L).userId(1L).walletId(1L).build();
		User existingUser=User.builder().userId(1L).build();
		Login login=Login.builder().user(existingUser).build();
		Product product=Product.builder().productId(123L).availableQuantity(5).cost(100D).build();
		ArrayList<CartProduct> cartProductList=new ArrayList<>();
		cartProductList.add(new CartProduct(1L, 123L, 1000));
		ArrayList<Product> products=new ArrayList<>();
		products.add(product);
		Cart cart=Cart.builder().cartId(1L).user(existingUser).cartProducts(cartProductList).build();
		Mockito.when(userRepository.findById(transactionDto.userId())).thenReturn(Optional.of(existingUser));
		Mockito.when(loginRepository.findByUserAndIsLogin(existingUser,true)).thenReturn(Optional.of(login));
		Mockito.when(cartRepository.findByCartIdAndUser(transactionDto.cartId(),existingUser)).thenReturn(Optional.of(cart));
		Mockito.when(productRepository.findAllById(anyList())).thenReturn(products);
		assertThrows(ResourceNotFoundException.class,()->transactionServiceImpl.purchaseProduct(transactionDto));	
	}
	
	@Test
	void testPurchaseProductWalletNotFound() {
		TransactionDto transactionDto=TransactionDto.builder().cartId(1L).userId(1L).walletId(1L).build();
		User existingUser=User.builder().userId(1L).build();
		Login login=Login.builder().user(existingUser).build();
		Product product=Product.builder().productId(123L).availableQuantity(5).cost(100D).build();
		ArrayList<CartProduct> cartProductList=new ArrayList<>();
		cartProductList.add(new CartProduct(1L, 123L, 1));
		ArrayList<Product> products=new ArrayList<>();
		products.add(product);
		Cart cart=Cart.builder().cartId(1L).user(existingUser).cartProducts(cartProductList).build();
		Mockito.when(userRepository.findById(transactionDto.userId())).thenReturn(Optional.of(existingUser));
		Mockito.when(loginRepository.findByUserAndIsLogin(existingUser,true)).thenReturn(Optional.of(login));
		Mockito.when(cartRepository.findByCartIdAndUser(transactionDto.cartId(),existingUser)).thenReturn(Optional.of(cart));
		Mockito.when(productRepository.findAllById(anyList())).thenReturn(products);
		Mockito.when(walletRepository.findByWalletIdAndUser(transactionDto.walletId(), existingUser)).thenReturn(Optional.empty());	
		assertThrows(ResourceNotFoundException.class,()->transactionServiceImpl.purchaseProduct(transactionDto));	
	}
	
	@Test
	void testPurchaseProductInsufficientBalance() {
		TransactionDto transactionDto=TransactionDto.builder().cartId(1L).userId(1L).walletId(1L).build();
		User existingUser=User.builder().userId(1L).build();
		Login login=Login.builder().user(existingUser).build();
		Product product=Product.builder().productId(123L).availableQuantity(5).cost(100D).build();
		ArrayList<CartProduct> cartProductList=new ArrayList<>();
		cartProductList.add(new CartProduct(1L, 123L, 1));
		ArrayList<Product> products=new ArrayList<>();
		products.add(product);
		Wallet wallet=Wallet.builder().walletId(1L).balance(50D).user(existingUser).build();
		Cart cart=Cart.builder().cartId(1L).user(existingUser).cartProducts(cartProductList).build();	
		Mockito.when(userRepository.findById(transactionDto.userId())).thenReturn(Optional.of(existingUser));
		Mockito.when(loginRepository.findByUserAndIsLogin(existingUser,true)).thenReturn(Optional.of(login));
		Mockito.when(cartRepository.findByCartIdAndUser(transactionDto.cartId(),existingUser)).thenReturn(Optional.of(cart));
		Mockito.when(productRepository.findAllById(anyList())).thenReturn(products);
		Mockito.when(walletRepository.findByWalletIdAndUser(transactionDto.walletId(), existingUser)).thenReturn(Optional.of(wallet));			
		assertThrows(InsufficientFundException.class,()->transactionServiceImpl.purchaseProduct(transactionDto));			
	}
	
	
	
}
