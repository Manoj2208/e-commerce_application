package com.hcl.ecommerce.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.ecommerce.dto.ApiResponse;
import com.hcl.ecommerce.dto.RegistrationDto;
import com.hcl.ecommerce.entity.User;
import com.hcl.ecommerce.exception.ResourceNotFoundException;
import com.hcl.ecommerce.repository.LoginRepository;
import com.hcl.ecommerce.repository.UserRepository;

@ExtendWith(SpringExtension.class)
class UserServiceImplTest {
	@Mock
	UserRepository userRepository;
	@Mock
	LoginRepository loginRepository;
	@InjectMocks
	UserServiceImpl userServiceImpl;
	@Test
	void testRegisterUserSuccess() {
		RegistrationDto registrationDto=new RegistrationDto("Vamsha", "vamshas@hclTech.com", "Vamsha@123");
		Mockito.when(userRepository.findByEmail(registrationDto.email())).thenReturn(Optional.empty());
		ApiResponse apiResponse=userServiceImpl.registerUser(registrationDto);
		assertEquals("User Registered Successfully", apiResponse.message());
		assertEquals(201L, apiResponse.httpStatus());
	}
	@Test
	void testRegisterUserAlreadyExist() {
		RegistrationDto registrationDto=new RegistrationDto("Vamsha", "vamshas@hclTech.com", "Vamsha@123");
		Mockito.when(userRepository.findByEmail(registrationDto.email())).thenReturn(Optional.of(User.builder().email("vamshas@hclTech.com").build()));
		assertThrows(ResourceNotFoundException.class, ()->userServiceImpl.registerUser(registrationDto));
	}
}
