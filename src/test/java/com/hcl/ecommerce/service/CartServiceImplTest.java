package com.hcl.ecommerce.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.ecommerce.dto.ApiResponse;
import com.hcl.ecommerce.dto.CartDto;
import com.hcl.ecommerce.dto.CartProductDto;
import com.hcl.ecommerce.entity.Cart;
import com.hcl.ecommerce.entity.CartProduct;
import com.hcl.ecommerce.entity.Login;
import com.hcl.ecommerce.entity.Product;
import com.hcl.ecommerce.entity.User;
import com.hcl.ecommerce.exception.ResourceNotFoundException;
import com.hcl.ecommerce.exception.UnauthorizedCustomer;
import com.hcl.ecommerce.repository.CartRepository;
import com.hcl.ecommerce.repository.LoginRepository;
import com.hcl.ecommerce.repository.ProductRepository;
import com.hcl.ecommerce.repository.UserRepository;
import com.hcl.ecommerce.service.impl.CartServiceImpl;

@ExtendWith(SpringExtension.class)
class CartServiceImplTest {
	@Mock
	private UserRepository userRepository;
	@Mock
	private LoginRepository loginRepository;
	@Mock
	private ProductRepository productRepository;
	@Mock
	private CartRepository cartRepository;
	@InjectMocks
	private CartServiceImpl cartServiceImpl;

	@Test
	void testAddToCart() {
		List<CartProductDto> cartProductDtos = List.of(new CartProductDto(12l, 10));
		CartDto cartDto = new CartDto(123l, cartProductDtos);
		List<CartProduct> cartProducts = List.of(new CartProduct(1l, 12l, 10));

		User user = User.builder().userId(123l).userName("Manoj").email("manoj@gmail.com").password("Manoj@2209")
				.registeredDate(LocalDateTime.now()).build();
		Login login = Login.builder().loginId(1l).isLogin(true).user(user).build();
		Product product = Product.builder().productId(12l).availableQuantity(12).cost(100.0).productName("Puma Shirt")
				.build();
		Mockito.when(userRepository.findById(cartDto.userId())).thenReturn(Optional.of(user));
		Mockito.when(loginRepository.findByUser(user)).thenReturn(login);
		Mockito.when(productRepository.findById(12l)).thenReturn(Optional.of(product));
		Cart cart = Cart.builder().cartId(1l).user(user).cartProducts(cartProducts).build();
		Mockito.when(cartRepository.save(cart)).thenReturn(cart);
		ApiResponse response = cartServiceImpl.addToCart(cartDto);
		assertNotNull(response);
		assertEquals("Products added to cart sucessfully", response.message());
	}

	@Test
	void testUserNotFound() {
		List<CartProductDto> cartProductDtos = List.of(new CartProductDto(12l, 10));
		CartDto cartDto = new CartDto(123l, cartProductDtos);
		Mockito.when(userRepository.findById(1l)).thenReturn(Optional.empty());
		assertThrows(ResourceNotFoundException.class, () -> cartServiceImpl.addToCart(cartDto));

	}

	@Test
	void testUserNotLoggedIn() {
		List<CartProductDto> cartProductDtos = List.of(new CartProductDto(12l, 10));
		CartDto cartDto = new CartDto(123l, cartProductDtos);
		User user = User.builder().userId(123l).userName("Manoj").email("manoj@gmail.com").password("Manoj@2209")
				.registeredDate(LocalDateTime.now()).build();
		Login login = Login.builder().loginId(1l).isLogin(false).user(user).build();
		Mockito.when(userRepository.findById(123l)).thenReturn(Optional.of(user));
		Mockito.when(loginRepository.findByUser(user)).thenReturn(login);
		assertThrows(UnauthorizedCustomer.class, () -> cartServiceImpl.addToCart(cartDto));
	}

	@Test
	void testProductNotFound() {
		List<CartProductDto> cartProductDtos = List.of(new CartProductDto(12l, 10));
		CartDto cartDto = new CartDto(123l, cartProductDtos);
		User user = User.builder().userId(123l).userName("Manoj").email("manoj@gmail.com").password("Manoj@2209")
				.registeredDate(LocalDateTime.now()).build();
		Login login = Login.builder().loginId(1l).isLogin(true).user(user).build();
		Mockito.when(userRepository.findById(123l)).thenReturn(Optional.of(user));
		Mockito.when(loginRepository.findByUser(user)).thenReturn(login);
		Mockito.when(productRepository.findById(1l)).thenReturn(Optional.empty());
		assertThrows(ResourceNotFoundException.class, () -> cartServiceImpl.addToCart(cartDto));
	}

}
