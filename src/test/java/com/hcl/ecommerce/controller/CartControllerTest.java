package com.hcl.ecommerce.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.ecommerce.dto.ApiResponse;
import com.hcl.ecommerce.dto.CartDto;
import com.hcl.ecommerce.dto.CartProductDto;
import com.hcl.ecommerce.service.CartService;

@ExtendWith(SpringExtension.class)
class CartControllerTest {
	@Mock
	private CartService cartService;
	@InjectMocks
	private CartController cartController;

	@Test
	void testAddToCart() {
		List<CartProductDto> cartProductDtos = List.of(new CartProductDto(12l, 10));
		CartDto cartDto = new CartDto(123l, cartProductDtos);
		ApiResponse apiResponse = new ApiResponse("success", 201l);
		Mockito.when(cartService.addToCart(cartDto)).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity = cartController.addToCart(cartDto);
		assertNotNull(responseEntity);
		assertEquals("success", responseEntity.getBody().message());
	}
}
