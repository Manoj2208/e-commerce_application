package com.hcl.ecommerce.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.ecommerce.dto.ApiResponse;
import com.hcl.ecommerce.dto.RegistrationDto;
import com.hcl.ecommerce.service.UserService;

@ExtendWith(SpringExtension.class)
class RegistrationControllerTest {
	@Mock
	UserService userService;
	@InjectMocks
	RegistrationController registrationController;
	@Test
	void testRegisterUser() {
		RegistrationDto registrationDto=new RegistrationDto("Vamsha", "vamshas@hclTech.com", "Vamsha@123");
		ApiResponse apiResponse=ApiResponse.builder().message("User Registered Successfully").httpStatus(201L).build();
		Mockito.when(userService.registerUser(registrationDto)).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity = registrationController.registerUser(registrationDto);
		assertNotNull(responseEntity);
		assertEquals(apiResponse.message(), responseEntity.getBody().message());
	}
}
