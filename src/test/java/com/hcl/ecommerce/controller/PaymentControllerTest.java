package com.hcl.ecommerce.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.ecommerce.dto.ApiResponse;
import com.hcl.ecommerce.dto.TransactionDto;
import com.hcl.ecommerce.service.TransactionService;

@ExtendWith(SpringExtension.class)
class PaymentControllerTest {
	@Mock
	TransactionService transactionService;
	@InjectMocks
	PaymentController paymentController;
	@Test
	void testPurchaseProduct() {
		TransactionDto transactionDto=TransactionDto.builder().cartId(1L).userId(1L).walletId(1L).build();
		ApiResponse apiResponse=ApiResponse.builder().message("Product purchased Successfully").httpStatus(201L).build();
		Mockito.when(transactionService.purchaseProduct(transactionDto)).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity=paymentController.purchaseProduct(transactionDto);
		assertEquals(apiResponse.httpStatus(),responseEntity.getBody().httpStatus());
		assertEquals(apiResponse.message(), responseEntity.getBody().message());
		assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
	}
}
