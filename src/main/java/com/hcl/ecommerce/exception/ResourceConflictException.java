package com.hcl.ecommerce.exception;

public class ResourceConflictException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ResourceConflictException() {
		super("Customer already Registered", GlobalErrorCode.ERROR_RESOURCE_CONFLICT_EXISTS);
	}

	public ResourceConflictException(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_CONFLICT_EXISTS);
	}

}
