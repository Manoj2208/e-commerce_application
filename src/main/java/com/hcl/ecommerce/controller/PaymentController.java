package com.hcl.ecommerce.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.ecommerce.dto.ApiResponse;
import com.hcl.ecommerce.dto.TransactionDto;
import com.hcl.ecommerce.service.TransactionService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/transaction")
@RequiredArgsConstructor
public class PaymentController {
	
	private final TransactionService transactionService;
	@PostMapping
	public ResponseEntity<ApiResponse> purchaseProduct(TransactionDto transactionDto){
		return new ResponseEntity<>(transactionService.purchaseProduct(transactionDto),HttpStatus.CREATED);
	}
}
