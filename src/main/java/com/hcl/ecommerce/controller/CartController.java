package com.hcl.ecommerce.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.ecommerce.dto.ApiResponse;
import com.hcl.ecommerce.dto.CartDto;
import com.hcl.ecommerce.service.CartService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/cart/products")
@RequiredArgsConstructor
@Tag(name = "Cart", description = "Endpoints for Managing carts")
public class CartController {
	private final CartService cartService;

	@PostMapping
	@Operation(summary = "Add to Cart", description = "Add Products to cart", tags = {"Cart"})
	public ResponseEntity<ApiResponse> addToCart(@RequestBody @Valid CartDto cartDto) {
		return ResponseEntity.status(HttpStatus.CREATED).body(cartService.addToCart(cartDto));
	}
}
