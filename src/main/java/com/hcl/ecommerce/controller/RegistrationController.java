package com.hcl.ecommerce.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.ecommerce.dto.ApiResponse;
import com.hcl.ecommerce.dto.RegistrationDto;
import com.hcl.ecommerce.service.UserService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class RegistrationController {
	
	private final UserService userService;

	@PostMapping
	public ResponseEntity<ApiResponse> registerUser(@RequestBody @Valid RegistrationDto registrationDto) {
		return new ResponseEntity<>(userService.registerUser(registrationDto), HttpStatus.CREATED);
	}
}
