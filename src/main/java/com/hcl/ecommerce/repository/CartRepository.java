package com.hcl.ecommerce.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.ecommerce.entity.Cart;
import com.hcl.ecommerce.entity.User;

public interface CartRepository extends JpaRepository<Cart,Long>{
	Optional<Cart> findByCartIdAndUser(Long cartId,User user);
}
