package com.hcl.ecommerce.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.ecommerce.entity.User;
import com.hcl.ecommerce.entity.Wallet;

public interface WalletRepository extends JpaRepository<Wallet, Long> {
	Optional<Wallet> findByWalletIdAndUser(Long walletId,User user);
}
