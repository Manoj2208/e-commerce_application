package com.hcl.ecommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.ecommerce.entity.CartProduct;

public interface CartProductRepository extends JpaRepository<CartProduct, Long> {

}
