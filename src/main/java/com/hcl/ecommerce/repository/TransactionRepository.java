package com.hcl.ecommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.ecommerce.entity.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Long>{

}
