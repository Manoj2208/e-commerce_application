package com.hcl.ecommerce.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.ecommerce.entity.Login;
import com.hcl.ecommerce.entity.User;


public interface LoginRepository extends JpaRepository<Login, Long>{
	Optional<Login> findByUserAndIsLogin(User user,boolean isLogin);

	Login findByUser(User user);

}
