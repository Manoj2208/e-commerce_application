package com.hcl.ecommerce.dto;

import lombok.Builder;

@Builder
public record TransactionDto(Long cartId,Long userId, Long walletId) {

}
