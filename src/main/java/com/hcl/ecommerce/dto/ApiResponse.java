package com.hcl.ecommerce.dto;

import lombok.Builder;

@Builder
public record ApiResponse(String message, Long httpStatus) {

}
