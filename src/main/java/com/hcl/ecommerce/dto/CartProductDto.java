package com.hcl.ecommerce.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;

public record CartProductDto(@NotNull(message = "productId is required") Long productId,
		@NotNull(message = "quantity is required field") @Min(value = 1, message = "Minimum quantity must be more than 0") Integer quantity) {
}
