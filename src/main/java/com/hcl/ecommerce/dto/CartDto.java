package com.hcl.ecommerce.dto;

import java.util.List;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

public record CartDto(@NotNull(message = "userId is required") Long userId,@Valid List<CartProductDto> cartProduct) {

}
