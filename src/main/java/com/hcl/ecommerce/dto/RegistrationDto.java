package com.hcl.ecommerce.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.Builder;

@Builder
public record RegistrationDto(@NotBlank(message = "userName is required") String userName,@NotBlank(message = "email is required") @Email(message = "invalid email pattern") String email,@NotBlank(message = "address is required") @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[#$@!%&*?])[A-Za-z\\d#$@!%&*?]{8,}$",message = "Min 1 uppercase letter. "
		+ "Min 1 lowercase letter.   "
		+ "Min 1 special character.  "
		+ "Min 1 number.  "
		+ "Min 8 characters. is required") String password) {

}
