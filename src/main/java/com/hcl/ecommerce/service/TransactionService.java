package com.hcl.ecommerce.service;

import com.hcl.ecommerce.dto.ApiResponse;
import com.hcl.ecommerce.dto.TransactionDto;

public interface TransactionService {
	ApiResponse purchaseProduct(TransactionDto transactionDto);
}
