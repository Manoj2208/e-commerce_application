package com.hcl.ecommerce.service;

import com.hcl.ecommerce.dto.ApiResponse;
import com.hcl.ecommerce.dto.CartDto;

public interface CartService {
	ApiResponse addToCart(CartDto cartDto);
}
