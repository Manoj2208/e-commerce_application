package com.hcl.ecommerce.service.impl;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.hcl.ecommerce.dto.ApiResponse;
import com.hcl.ecommerce.dto.RegistrationDto;
import com.hcl.ecommerce.entity.Login;
import com.hcl.ecommerce.entity.User;
import com.hcl.ecommerce.exception.ResourceNotFoundException;
import com.hcl.ecommerce.repository.LoginRepository;
import com.hcl.ecommerce.repository.UserRepository;
import com.hcl.ecommerce.service.UserService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService{
	
	private final UserRepository userRepository;
	private final LoginRepository loginRepository;

	@Override
	public ApiResponse registerUser(RegistrationDto registrationDto) {
		log.info("Check existing user EmailId");
		Optional<User> existingUser=userRepository.findByEmail(registrationDto.email());
		if(existingUser.isPresent()) {
			log.warn("User Already Exist");
			throw new ResourceNotFoundException("User Already Exist");
		}
		log.info("Create new User");
		User user=new User();
		BeanUtils.copyProperties(registrationDto, user);
		user.setRegisteredDate(LocalDateTime.now());
		userRepository.save(user);
		log.info("User successfully registered");
		Login login=Login.builder().isLogin(false).user(user).build();
		loginRepository.save(login);
		return new ApiResponse("User Registered Successfully", 201L);
	
	}
	
}
