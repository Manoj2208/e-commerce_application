package com.hcl.ecommerce.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.hcl.ecommerce.dto.ApiResponse;
import com.hcl.ecommerce.dto.CartDto;
import com.hcl.ecommerce.entity.Cart;
import com.hcl.ecommerce.entity.CartProduct;
import com.hcl.ecommerce.entity.Login;
import com.hcl.ecommerce.entity.User;
import com.hcl.ecommerce.exception.ResourceNotFoundException;
import com.hcl.ecommerce.exception.UnauthorizedCustomer;
import com.hcl.ecommerce.repository.CartRepository;
import com.hcl.ecommerce.repository.LoginRepository;
import com.hcl.ecommerce.repository.ProductRepository;
import com.hcl.ecommerce.repository.UserRepository;
import com.hcl.ecommerce.service.CartService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class CartServiceImpl implements CartService {
	private final UserRepository userRepository;
	private final CartRepository cartRepository;
	private final LoginRepository loginRepository;
	private final ProductRepository productRepository;
	

	@Override
	public ApiResponse addToCart(CartDto cartDto) {
		log.warn("Invalid userId causes UserNotFound Exception");
		User user = userRepository.findById(cartDto.userId())
				.orElseThrow(() -> new ResourceNotFoundException("User Not Found"));
		Login login = loginRepository.findByUser(user);
		if (login.isLogin()) {
			log.warn("Invalid productId causes ProductNotFound Exception");
			cartDto.cartProduct().forEach(cartProduct -> productRepository.findById(cartProduct.productId())
					.orElseThrow(() -> new ResourceNotFoundException("Product Not Found")));
			List<CartProduct> cartProducts = cartDto.cartProduct().stream().map(cartProduct -> CartProduct.builder()
					.productId(cartProduct.productId()).quantity(cartProduct.quantity()).build()).toList();
			Cart cart = Cart.builder().user(user).cartProducts(cartProducts).build();
			cartRepository.save(cart);
			log.info("Products added to cart successfully");
			return new ApiResponse("Products added to cart sucessfully", 201l);
		}
		log.error("Unauthorized customer due to customer not logged in");
		throw new UnauthorizedCustomer();
	}

}
