package com.hcl.ecommerce.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.hcl.ecommerce.dto.ApiResponse;
import com.hcl.ecommerce.dto.TransactionDto;
import com.hcl.ecommerce.entity.Cart;
import com.hcl.ecommerce.entity.CartProduct;
import com.hcl.ecommerce.entity.Login;
import com.hcl.ecommerce.entity.Product;
import com.hcl.ecommerce.entity.Transaction;
import com.hcl.ecommerce.entity.User;
import com.hcl.ecommerce.entity.Wallet;
import com.hcl.ecommerce.exception.InsufficientFundException;
import com.hcl.ecommerce.exception.ResourceNotFoundException;
import com.hcl.ecommerce.exception.UnauthorizedCustomer;
import com.hcl.ecommerce.repository.CartRepository;
import com.hcl.ecommerce.repository.LoginRepository;
import com.hcl.ecommerce.repository.ProductRepository;
import com.hcl.ecommerce.repository.TransactionRepository;
import com.hcl.ecommerce.repository.UserRepository;
import com.hcl.ecommerce.repository.WalletRepository;
import com.hcl.ecommerce.service.TransactionService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class TransactionServiceImpl implements TransactionService{
	private final UserRepository userRepository;
	private final LoginRepository loginRepository;
	private final WalletRepository walletRepository;
	private final ProductRepository productRepository;
	private final CartRepository cartRepository;
	private final TransactionRepository transactionRepository;
	@Override
	public ApiResponse purchaseProduct(TransactionDto transactionDto) {
		User existingUser=userRepository.findById(transactionDto.userId()).orElseThrow(()->new ResourceNotFoundException("User not found"));
		Optional<Login> login= loginRepository.findByUserAndIsLogin(existingUser,true);
		if(login.isPresent()) {
			log.warn("Authorized User");
			Cart cart=cartRepository.findByCartIdAndUser(transactionDto.cartId(),existingUser).orElseThrow(()->new ResourceNotFoundException("Cart not available"));
			List<CartProduct> cartProducts=cart.getCartProducts();
			List<Long> productIds=cartProducts.stream().map(CartProduct::getProductId).toList();
			List<Product> products=productRepository.findAllById(productIds);
			Map<Long, Product> productsMap=products.stream().collect(Collectors.toMap(Product::getProductId, Function.identity()));
			
			Double totalPrice=cartProducts.stream().mapToDouble(cartproduct->{
				if(productsMap.get(cartproduct.getProductId()).getAvailableQuantity()- cartproduct.getQuantity()<=0) {
					log.warn("Requested quantity of product is unavailable");
					throw new ResourceNotFoundException("Requested quantity of product is unavailable");
				}
				return productsMap.get(cartproduct.getProductId()).getCost()*cartproduct.getQuantity();
			}).sum();
			
			Wallet wallet= walletRepository.findByWalletIdAndUser(transactionDto.walletId(), existingUser).orElseThrow(()->new ResourceNotFoundException("Invalid Wallet id"));
			if(wallet.getBalance()-totalPrice<=0) {
				log.warn("Insufficient Balance in the wallet");
				throw new InsufficientFundException();
			}
			
		    
			List<Product> updatedProducts=cartProducts.stream().map(cartproduct->{			
				productsMap.get(cartproduct.getProductId()).setAvailableQuantity(productsMap.get(cartproduct.getProductId()).getAvailableQuantity()-cartproduct.getQuantity());
				return productsMap.get(cartproduct.getProductId());
			}).toList();
			
			wallet.setBalance(wallet.getBalance()-totalPrice);
			Transaction transaction=Transaction.builder().amount(totalPrice).cartId(transactionDto.cartId()).transactionAt(LocalDate.now()).user(existingUser).build();
			transactionRepository.save(transaction);
			productRepository.saveAll(updatedProducts);
			walletRepository.save(wallet);
			log.info("product purchased successfully");
			return new ApiResponse("Product purchased Successfully",201L);
			
		}
		log.warn("User not looged In");
		throw new UnauthorizedCustomer();
		
	}

}
