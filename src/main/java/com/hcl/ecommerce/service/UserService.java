package com.hcl.ecommerce.service;

import com.hcl.ecommerce.dto.ApiResponse;
import com.hcl.ecommerce.dto.RegistrationDto;

public interface UserService {
	public ApiResponse registerUser(RegistrationDto registrationDto);
}
